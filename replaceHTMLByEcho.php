<?php
/**
 * @param string $folderNameSource
 * @param string $folderNameResult
 * @return array
 */
function ReplaceHTMLByEcho(string $folderNameSource, string $folderNameResult)
{
    $iteratorSafe = new DirectoryIterator($folderNameSource);
// On boucle sur la liste des documents retournés dans l'itérateur
    foreach ($iteratorSafe as $document) {
        if ($document->getType() == 'file') {
            echo 'Nom du document: ' . $document->getFilename(), PHP_EOL;
            echo 'Path: ' . $document->getPath(), PHP_EOL;
            echo 'Path: ' . $document->getPathname(), PHP_EOL;
            echo 'Taille du document en octets:  ' . $document->getSize(), PHP_EOL;
//        echo 'Type de document (file, dir ou link):  ' . $document->getType(), PHP_EOL;
            $result = retrievePHPCodeAndEchoHTML($document->getPathname());
            writeStringInFile($folderNameResult . "/" . $document->getFilename(), $result);
        }

    }
}
