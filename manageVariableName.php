<?php
include "randomChar.php";


function generateVariableName(){
    $path = 'variable';
    $handle = file_get_contents($path, "r");
    $handle = unserialize($handle);
    $resultGeneratorKey = array();
    $resultRandomKey = array();
    foreach ($handle[2] as $varName ) {
        //echo  $varName ."\n";
        $c = uniqid (rand(),true); //creates a unique ID with a random number as a prefix - more secure than a static prefix
        $md5c =  "$". generatePrefixVariableName() .md5($c); //this md5 encrypts the username from above, so its ready to be stored in your database
        $resultGeneratorKey[$varName] =$md5c;
        $resultRandomKey[$md5c] = "$".$varName;
        //print_r($resultGeneratorKey);
        //print_r($resultRandomKey);
    }
    return array($resultGeneratorKey, $resultRandomKey);
}

