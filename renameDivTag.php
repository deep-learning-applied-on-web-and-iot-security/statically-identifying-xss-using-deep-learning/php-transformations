<?php

function renameDivTag($tokens, $stringTag){
    $renamedTrue = False;
    $result ="";
    foreach ($tokens as $token) {
        //print_r( $token );
        if (is_array($token)) {
            if(token_name($token[0]) == "T_CONSTANT_ENCAPSED_STRING"){
//                echo "HEEEERRRREEEE " . $token[1] ."\n";
                switch ($token[1]) {
                    case "\"<div id=\"":
//                        echo "I'm div but I'm renamed" ."\n";
                        $token[1] = "\"<$stringTag id=\"";
                        $renamedTrue = $renamedTrue ? True : True;
                        break;
                    case "\">content</div>\"":
//                        echo "I'm end div but I'm renamed" ."\n";
                        $token[1] = "\">content</$stringTag>\"";
                        $renamedTrue = $renamedTrue ? True : True;
                        break;
                    case "\"<div id='\"":
//                        echo "I'm div but I'm renamed" ."\n";
                        $token[1] = '"<'. $stringTag .' id=\'"';
                        $renamedTrue =  $renamedTrue ? True : True;
                        break;
                    case "\"'>content</div>\"":
//                        echo "I'm array but I'm renamed" ."\n";
                        $token[1] = '"\'>content</'. $stringTag. '>"';
                        $renamedTrue =  $renamedTrue ? True : True;
                        break;
                    case '"<div id=\""':
//                        echo "I'm div but I'm renamed" ."\n";
                        $token[1] = '"<'. $stringTag. ' id=\""';
                        $renamedTrue = $renamedTrue ? True : True;
                        break;
                    case '"\">content</div>"':
//                        echo "I'm div but I'm renamed" ."\n";
                        $token[1] = '"\">content</'. $stringTag .'>"';
                        $renamedTrue = $renamedTrue ? True : True;
                        break;
                }
            }
            elseif (token_name($token[0]) == "T_INLINE_HTML"){
//                echo "HEEEERRRREEEE " . $token[1] ."\n";
//                echo "FIN " ."\n";
                $list_html_tag = preg_split("/[\n]+/", $token[1]);
//                print_r($list_html_tag);
                if(in_array("<div>", $list_html_tag)){
                    $res = "";
                    foreach ($list_html_tag as $element){
                        if($element == "<div>"){
                            $res = $res . "\n" .'<'. $stringTag. '>' ;
                        }
                        else{

                            $res = $res  . $element . "\n";
                        }
                    }
                    $token[1] = $res;
                    $renamedTrue = $renamedTrue ? True : True;
//                    echo $token[1];
                }
                elseif (in_array("</div>", $list_html_tag)){
                    $res = "";
                    foreach ($list_html_tag as $element){
                        if($element == "</div>"){
                            $res = $res . '</'. $stringTag. '>';
                        }
                        else{
//                            echo $element;
                            $res = $res . "\n" . $element;
                        }
                    }
                    $token[1] = $res;
                    $renamedTrue = $renamedTrue ? True : True;
//                    echo $token[1];
                }
            }
            echo "Line {$token[2]}: ", token_name($token[0]), " ('{$token[1]}')", PHP_EOL;
            $result = $result . $token[1];
        }
        else{
            echo $token, PHP_EOL;
            $result = $result . $token;
        }
    }
    return array($result, $renamedTrue);
}
