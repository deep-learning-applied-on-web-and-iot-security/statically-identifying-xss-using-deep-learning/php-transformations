<?php
include '../manageFolder.php';
include 'renameVariableRandomly.php';
include 'renameDivTag.php';


$FOLDER_NAME_RESULT = "/absolute/path/PHP-D1/";
$FOLDER_NAME_SAFE_RESULT = "/absolute/path/PHP-D1/safe";
$FOLDER_NAME_UNSAFE_RESULT = "/absolute/path/PHP-D1/unsafe";
createFolder($FOLDER_NAME_RESULT); //only if it's not exist
createFolder($FOLDER_NAME_SAFE_RESULT); //only if it's not exist
createFolder($FOLDER_NAME_UNSAFE_RESULT); //only if it's not exist

$FOLDER_NAME_SAFE ="/absolute/path/PHP-D0/safe";
$FOLDER_NAME_UNSAFE ="/absolute/path/PHP-D0/unsafe";
//$FOLDER_NAME_SAFE = '../php-data-test';
//$FOLDER_NAME_SAFE = '../php-data-test';
// Script start
$rustart = getrusage();

$iterator = new DirectoryIterator($FOLDER_NAME_SAFE);


$divRenamedByArray = ["p", "dl", "figure", "ul","canvas", "video", "ol", "span", "pre", "h1", "h2", "h3", "h4", "h5","section", "article", "main", "aside", "footer", "nav", "blockquote", "form", "address"];
$divRenamedByArray = ["p"];
$count = 1;
$seed = 5+ $count;
srand(5+$count);




foreach ($divRenamedByArray as $divRenamedBy){
    // On boucle sur la liste des documents retournés dans l'itérateur
    foreach($iterator as $document) {
        if($document->getType() == 'file'){
            echo 'Nom du document: ' . $document->getFilename(), PHP_EOL;
            echo 'Path: ' . $document->getPath(), PHP_EOL;
            echo 'Path: ' . $document->getPathname(), PHP_EOL;
            echo 'Taille du document en octets:  ' . $document->getSize(), PHP_EOL;
//        echo 'Type de document (file, dir ou link):  ' . $document->getType(), PHP_EOL;
            $result = renameVariablePHPCodeRandomly( $document->getPathname());
            $result = token_get_all($result);
//            $divRenamedBy = "p";
            $result = renameDivTag($result, $divRenamedBy); 
            //echo $result;
            if ($result[1]){
                $resultPath = $FOLDER_NAME_SAFE_RESULT . "/" . "renamedVariable" . $count. "&".  "divRenamedBy_" . $divRenamedBy ."__". $document->getFilename();
            }else {
                $resultPath = $FOLDER_NAME_SAFE_RESULT . "/" . "renamedVariable" . $count. "__".$document->getFilename();

            }
            writeStringInFile($resultPath  , $result[0]);
        }

    }
    $count++;
}
srand(5+$count);

$iterator = new DirectoryIterator($FOLDER_NAME_UNSAFE);


foreach ($divRenamedByArray as $divRenamedBy){
    // On boucle sur la liste des documents retournés dans l'itérateur
    foreach($iterator as $document) {
        if($document->getType() == 'file'){
            echo 'Nom du document: ' . $document->getFilename(), PHP_EOL;
            echo 'Path: ' . $document->getPath(), PHP_EOL;
            echo 'Path: ' . $document->getPathname(), PHP_EOL;
            echo 'Taille du document en octets:  ' . $document->getSize(), PHP_EOL;
//        echo 'Type de document (file, dir ou link):  ' . $document->getType(), PHP_EOL;
            $result = renameVariablePHPCodeRandomly( $document->getPathname());
            $result = token_get_all($result);
//            $divRenamedBy = "p";
            $result = renameDivTag($result, $divRenamedBy);
            //echo $result;
            if ($result[1]){
                $resultPath = $FOLDER_NAME_UNSAFE_RESULT . "/" . "renamedVariable" . $count. "&".  "divRenamedBy_" . $divRenamedBy ."__". $document->getFilename();
            }else {
                $resultPath = $FOLDER_NAME_UNSAFE_RESULT . "/" . "renamedVariable" . $count. "__".$document->getFilename();

            }
            writeStringInFile($resultPath  , $result[0]);
        }

    }
    $count++;
}



// Code ...

// Script end
function rutime($ru, $rus, $index) {
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
        -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

$ru = getrusage();
echo "This process used " . rutime($ru, $rustart, "utime") .
    " ms for its computations\n";
echo "It spent " . rutime($ru, $rustart, "stime") .
    " ms in system calls\n";
?>
