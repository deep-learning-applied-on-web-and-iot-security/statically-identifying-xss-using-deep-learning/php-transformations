<?php
/** create the folder if it doesn't exist
 * @param $folderPath relative path to access to the folder
 */
function createFolder($folderPath, $recursive =FALSE){
    if(!is_dir($folderPath)){
        mkdir($folderPath,$mode=0777,$recursive=$recursive);
    }
}

/**
 * @param $filePath the path of the file where you would like read the string
 * @param $string the text that you would like put in a file
 */
function writeStringInFile($filePath, $string){
    $result = fopen($filePath, "c");
    fwrite($result, $string);
    fclose($result);
}
?>